#!/bin/bash

# Set the API endpoint URL
API_URL="http://localhost:4000/todos"

# Create a new to-do list item
echo "Creating a new to-do list item..."
curl -X POST \
  $API_URL \
  -H 'Content-Type: application/json' \
  -d '{"title": "Buy milk", "completed": false}'

curl -X POST \
  $API_URL \
  -H 'Content-Type: application/json' \
  -d '{"title": "Buy cheese", "completed": false}'

# Retrieve all to-do list items
echo "Retrieving all to-do list items..."
curl -X GET \
  $API_URL

# Retrieve a single to-do list item by ID
echo "Retrieving a single to-do list item by ID..."
curl -X GET \
  $API_URL/1

# Update a single to-do list item by ID
echo "Updating a single to-do list item by ID..."
curl -X PUT \
  $API_URL/1 \
  -H 'Content-Type: application/json' \
  -d '{"title": "Buy eggs", "completed": true}'

# Retrieve all to-do list items
echo "Retrieving all to-do list items..."
curl -X GET \
  $API_URL

# Delete a single to-do list item by ID
echo "Deleting a single to-do list item by ID..."
curl -X DELETE \
  $API_URL/1

# Retrieve all to-do list items
echo "Retrieving all to-do list items..."
curl -X GET \
  $API_URL

echo "Deleting a single to-do list item by ID..."
curl -X DELETE \
  $API_URL/2


echo "Testing complete!"