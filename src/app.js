const express = require('express');
const app = express();
const todos = [];

app.use(express.json());

// GET endpoint to fetch all todo items
app.get('/todos', (req, res) => {
  res.json(todos);
});

// POST endpoint to create a new todo item
app.post('/todos', (req, res) => {
  const todo = {
    id: todos.length + 1,
    title: req.body.title,
    completed: req.body.completed || false,
  };
  todos.push(todo);
  res.status(201).json(todo);
});

// GET endpoint to fetch a single todo item
app.get('/todos/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const todo = todos.find((todo) => todo.id === id);
  if (!todo) {
    res.status(404).json({ error: 'Todo not found' });
  } else {
    res.json(todo);
  }
});

// PUT endpoint to update a todo item
app.put('/todos/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const todo = todos.find((todo) => todo.id === id);
  if (!todo) {
    res.status(404).json({ error: 'Todo not found' });
  } else {
    todo.title = req.body.title;
    todo.completed = req.body.completed;
    res.json(todo);
  }
});

// DELETE endpoint to delete a todo item
app.delete('/todos/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const index = todos.findIndex((todo) => todo.id === id);
  if (index === -1) {
    res.status(404).json({ error: 'Todo not found' });
  } else {
    todos.splice(index, 1);
    res.json({ message: 'Todo deleted successfully' });
  }
});

app.listen(4000, () => {
  console.log('Server listening on port 4000');
});